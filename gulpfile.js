'use strict';

let fs = require('fs');
let cp = require('child_process');
let gulp = require('gulp');

const readMeUrl = 'https://raw.githubusercontent.com/fatihacet/turkcekaynaklar-com/master/README.md';
const contentRegex = /-----(.|\n+)+/;
const emojis = {
  ':movie_camera:': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEgAAABICAYAAABV7bNHAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAALEwAACxMBAJqcGAAAC/FJREFUeNrtm3twXNV9xz/n3LtPSauVkK2XX/IrYAKyKZACA9O0hGmGpC/6nHQoTDuU1Ck0nQyTdiZtM03ppGkyMYW0cSl02iRtnKaQDkmbBiYhwcQIsL16GdkSliVLu6vVY9+Pu/fe0z/uruqHpN3Vy5joO3NnNFe/e76/8z2/8zvPhQ1sYAMbuHIQq1HIw4cOV237D48+tOqVWEv+FQl0iWMC0IEA4C+9U0ASyALmaou0CH8DUHfB+ySQuZC/Fh+WJdC8Y0ppQtN2CyEOKKVuR6l9CpoB9wXmWQFjCPmmEKJHKdWrlB1DqWWLVeZXSkmpaTtL/Leh1HUKNgOeC8xzAs4jxHEh5GtK2b1ARNl2Vfw1CXRBi7mFkDcjuN82i+8vZNK7cvG4lp2boZBKYBrG/DcujxdPoJG65ha8gUDK7a8PSd31TQT/aRnGmNS0qkUq8wshdQQHgPutYvHnjEx6dy4Rd+Xis+STCUyjMP+N7vHgbWjE33wNvkAw466r75e6/hzwDam73rZKtov5ULVADx86jJASpdRuIcRBs1D4zWRksm323AjJSBgjk8K2bZRtU46OUm0QUiI1DW9DgEB7J9ds32XVbdp8Und7n0SpbyhUBqWWFOrhQ/8IAgRsBz5aLOR/Oxme6JwbHy3xp7Fta3F+qeFpCNDY3knz9p12/abWft3jOYxSX1UQX4xfq1accH+Ihtb2u5Vl/V1i8vx94yd6Gib7jpOKRrAKeVTJKSHEZQ+Asm2KuSzpWJS58XMyn4x3uLzeu93+unap6SGp68nXv/38ovxSdwHqTtuynkiEJ35r7I1jjeGBk6SmquRXNmaZf+KcyCfjrS6f/2fd/rodUtN6pe6aW4i/YgSVwloXUvuNYj77l9FT/V3hwV6MbGaevGYohQK8DQE63rtfbdpz7Utuf92jCga/9AcPLsQvpaZ9uJBOfzY61P+e6Kl+CivkB/A0BOi44QCbdl/7I7fP/4iCk186eDH/khF08KlnUZYlhabfl0/Gvzj2+qtbIqf6sIzC8p0DJ+yFwCwUSEYmhGkYO/1NzftcHt/RW+/9pdlyS37qhZfJzs0IqWk/n0vEnxp788e7IoP9q8LPPP8ktmVu9wWb9usez7Fb7v3F2Ovf+da8qVyqHFvZSF2/LZ+MPz7ac7Q1NvyW08dX4txFfgpsyyJ6qo/x4z0/k0vM/bnUtOY/+ucjAMRGhhBSO5BLJj537vVXt8WGh1BqlflNk8n+k4yf6PnpQjr1uBCy7dGnv1ZZoE985TmklJuMXPZPJ3qP75odHWGV5pWXQSnF1JlTIjwQ+tV8MvGA2+fXHvv6t9HdnqCRTX9yIvTG9TOjw2vCXXKA2Jm3CA/0ftDIph+SLpdezmn6Yt9Il0tYxeJHpkdO3x0bfmvtnCv7aFlMnR70egPBj7n8/pdSU5GQv+maX5keOf2h2PCQM+VcQ9imSfStPpcvGPx9t6/ufx999sgxWCSCDp2OkIvPbU/Hog9EBnvdVrG45gIhBMV8nujQwI74+XMf2bRrb3sqOvlg5FSfz15P/lP9Hcno5INer9e9qEDX72nFyGbumR45/d5cIr6yhFiTj4LsTExMj5z5UHo69tD028M/lU8mVi3nVMOfjkWZPjv8wdRM7PrDk7mFBXrle8c8qanwL8yNn9MumnStA2zbJhWL7J0dO/vQ3Pio50rwJybGt8Qnxu7c1O4VCw7z+953x57o0MAjc+OjzTWVrhQIgdQ0hKY7La8UTgKpLgqEEJiGIQvJREM+Ga8tdC7hF0I4E8jS+2r5LaMgPHUNOa1+8/MLJul8KrE7GQ232bZdffdSCnd9Ay1duwm0d+L211FIpYhPjDEzOoKZz1XtpLIs0tNTNWmDUrj8dVyzYyeNndvw1NVTyKRJTJ5n9uwwRjZTNb9tWaRikb1nf/xys345j+LX//iT7cVctq6q0krO+YLNdN12F43tnQjp9Nz6ls00bdtBoL2Tcz1HMTLptcknSuFpCNB1210Et2xHXsDfvHUHwY4tnH3tFQo15LPs7EwwF493LZiDMtNTrWY+X/WsR3O72XrTrQQ7t0IprMuPkJKWnXvouOEAQqtq6VczpK7TeeNNNG/rmu9W5QchaNq+k637b0G6XFWXaVtmIBEe37aQQCKfTOhW0ahKbaUU/uYWGju2LD5VEYLmbV146htY9aRbit6mbV2L8ytFcOt2fMHm+UVtxWJtW88nE/6FBFKFTNq0LatqH/1NzWgu9+KVVwrd68UXCK76fE+h8AWb0N2eJcXXXG58jcHqy7Vtdz6ZrF+wi+XiszVlSCFklYarrE6tZdfQOkopZZtF+7KaOX2YQi2Tw8xMDMtYoksKQTGXIxufW3WNBILs7AzFfH5JfqtokIvP1iJQ2jKLEws2vcvrTSBlvioHheNgfGJsKTZmRkfWZhQTglwiztzY2SW7+OzoCDWtCpRKW4YRXlAgs5AfUbYdqdZHyywyfqKHufHR+ZFLSIkQEmVbxIaHCA+EUHb1ea0WKNtisu8EM6MjKNtGCDnvg7JtZs4OMxF6E9usaU2XBM4tOO5ubmnJAB/G2f+tCmY+TzIy6ewNmybFXIZ0LEp4oJfwYAgzV/1EcTmwnM038skktm2VtneniAz2Ee4/WdNEsYQXgX9b8Ivu7m4BfBr4VE1elrZSpaYhdRd20cCZjUONGdoCJoAtVNjUW5LfLGJblsNcmzgW8FHg6cXIFfA/QKwmgUpbqcq2sYyC092EqFUcgLPAXwPVZ9WF+MtLpdojdwT4IaAWFCgUCgGcBL5Xa8mrAFXi/TpOmF8J/DdwJhQKLRm+WeCfgBpXjSvGGPAsMAc8A8ysM//bJV4blujfpSj6Ucl4bYafy1EAngJOlPh/CPzrOvIbwJPAQIm/YgIslj747jo4p4Bv4kRt+aJBAfgC69PVbOAITvTON8iSy+toNEpbW1sK6ANuATrW0MEXgceAiXLrlfiTQC9wM9C5RtwKJwg+AUTK/FD9ENoHfAx4ZY0c/C7wCHBmkf/3AgeBY2vE/wLwcWD00n+I7u7u0p8KITW8DY3c8XsH+fz9vzZv9P827AX+AvhlwLsKjiWBrwGPA+NV2O8D/gxnEuuvwr4S0sBXgL8CzsN87p2H1tbWJoD3gfhDICAgGtzalbvullvp6OxkZKCvHOrgjCgvlQrbArRQy0TuYhSBHwDfB1qBG4EbKjybceZmTcBWVrY/MA38CfA3wGwoFCIajV5mpOMsJ74A3C6EyLRed8NrZj57pJjLfmvfvfdFmnfsJpeM8/zfPwFAd3d3HPgyTlL7IkscPlaAC/gAcE+NFS2vSJfbMBfydwGuUCiUW8xIa2tr+wDwu4BXCOFu2bWnq7Fj6z0ur//OzPRUvZHNTP7HE3+bPBmexpXL0NffT1tbWyPwGZwutxJopYou51kpvMB+4Hg0Gh1azEji3Om7aLNWaprb5fPd7g00fr6htf2/fufTn/24su1Ol9dbzkedwHtWwckrDT9w01IGS3YPIaXu8vr2S02/MTM7fZ9VLB4WUn6H1WnBdwIEUL9sgeZLkVJaxeIdCHEz8CrOcFv9EcFVjFoTrAd4P3AX754oWhLLHYHW5oDrHYifiChYCTYEqoANgSpgQ6AK2BCoAjYEqoANgSpgQ6AK2BCoAjYEqoDlCDSFsxOYvtLOrweqXotJKWdR6gWEeAbIAc9RYavg3YCKAtmmmSnmcy97G4NPGkbh+x6vL4+zf/wTAR3nNNEuvxAASmGbZs40Cj8wMulncom5F6Xuimvachf/Vy904BTOSUE9gGkUZoxM5mghm/6X9FTkpZZde+NmPs/5Ez0MDA4CFx0Dveuh4xwKHgIeQKmj6anoEbevrqf12uvz+cQcTz/2yJX28YoLVD6w/6plmnGp6abu8fLUJb8dvQTr8/ObdwBk6STRAqalppmxkSH+/XOfqfRdCufg72qHosL1Gh0uP26tApM4V2OqvsP4DkUE52R3USx3WMrj3GEs4ty6uNpm5AIncr4MvFHJcFkojWQunPP5q3H8TwIJWLoHrbRiRSB8pWu6EiwjvWxgAxvYwLrh/wA0s3Gx6B3dJAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxNi0wNy0wN1QwOTo0MTozOS0wNzowMDdY/bEAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTYtMDctMDdUMDk6NDE6MzktMDc6MDBGBUUNAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAABJRU5ErkJggg==',
};
const header = `
<h2>Konu başlıkları</h2>

* TOC
{:toc}
`

gulp.task('default', (done) => {
  setTimeout(() => {
    cp.exec(`curl ${readMeUrl}`, (err, stdout) => {
      if (err) return;

      const rawContent = stdout;
      let [ content ] = rawContent.match(contentRegex);

      for (let key in emojis) {
        const img = emojis[key];
        const regex = new RegExp(key, 'g');
        content = content.replace(regex, `![${key}](${img}){:class="emoji"}`);
      }

      fs.writeFileSync('./_includes/main_content.md', `${header}${content}`);
      fs.writeFileSync('./README.md', rawContent);

      done();
    });
  }, 60000);
});
